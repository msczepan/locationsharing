package com.locationsharingapp.locationsharingapp;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by dstroynov on 14.12.15.
 */
public class UserLocation {
    private String displayName;
    private LatLng coords;
    private Marker marker;

    public UserLocation(String displayName, LatLng coords, Marker marker) {
        this.displayName = displayName;
        this.coords = coords;
        this.marker = marker;
    }

    public UserLocation(String displayName, LatLng coords) {
        this.displayName = displayName;
        this.coords = coords;
    }

    public UserLocation(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public LatLng getCoords() {
        return coords;
    }

    public void setCoords(LatLng coords) {
        this.coords = coords;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
