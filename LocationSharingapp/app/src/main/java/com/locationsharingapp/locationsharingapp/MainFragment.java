package com.locationsharingapp.locationsharingapp;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.OnMapReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MainFragment extends Fragment implements OnGetCoordsTaskCompleted, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 9001;
    private static final float DEFAULT_ZOOM_LEVEL = 17;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    private GoogleSignInAccount mGoogleSignInAccount;
    private UserLocation myLocation;
    private UserLocations locations;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        buildGoogleApiClient();
    }

    // Create an instance of GoogleAPIClient
    private void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addConnectionCallbacks(this)
                    .build();
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void updatePositionOnMap(UserLocation userLocation) {
        if (mMap != null) {
            Marker marker = userLocation.getMarker();

            if (marker == null) {
                marker = mMap.addMarker(new MarkerOptions()
                                .position(userLocation.getCoords())
                                .title(userLocation.getDisplayName())
                );
                userLocation.setMarker(marker);
            } else {
                marker.setPosition(userLocation.getCoords());
            }
        }
    }

    public void moveCameraToMyPosition() {
        if (mMap != null && myLocation != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation.getCoords(), DEFAULT_ZOOM_LEVEL));
        }
    }

    private void initializeMyLocation() {
        if (myLocation == null) {
            if (mGoogleApiClient != null && mGoogleSignInAccount != null) {
                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient
                );

                if (mLastLocation != null) {
                    myLocation = new UserLocation(mGoogleSignInAccount.getDisplayName());
                    myLocation.setCoords(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                    updatePositionOnMap(myLocation);
                    myLocation.getMarker().setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    moveCameraToMyPosition();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (locations != null) {
            Enumeration e = locations.keys();
            while (e.hasMoreElements()) {
                String el = (String) e.nextElement();
                locations.get(el).setMarker(null);
                updatePositionOnMap(locations.get(el));
            }
        }
    }

    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        signIn();
        }
    }

    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess() && mGoogleSignInAccount == null) {
            mGoogleSignInAccount = result.getSignInAccount();
            locations = new UserLocations();
            runTimer();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void onConnectionSuspended(int var1) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult var1) {
    }

    public void onConnected(Bundle connectionHint) {
        initializeMyLocation();
        LocationRequest request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(1000);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, request, new LocationListener() {

                    @Override
                    public void onLocationChanged(Location location) {
                        if (myLocation != null) {
                            myLocation.setCoords(new LatLng(location.getLatitude(), location.getLongitude()));
                            updatePositionOnMap(myLocation);
                            postMyCoords(myLocation);
                        } else {
                            initializeMyLocation();
                        }
                    }
                });
    }

    private void runTimer() {
        final Handler h = new Handler();
        final int delay = 10000; //milliseconds

        h.postDelayed(new Runnable() {
            public void run() {
                getUserCoords();
                h.postDelayed(this, delay);
            }
        }, delay);
        getUserCoords();
    }

    private void getUserCoords() {
        new GetCoordsTask(this).execute(locations);
    }

    private void postMyCoords(UserLocation myLocation) {
        new PostCoordsTask().execute(myLocation);
    }

    public void onGetCoordsTaskCompleted(JSONArray jsonArray) {
        try {
            int l = jsonArray.length();
            List<String> names = new ArrayList<>();

            for (int i = 0; i < l; i++) {
                String displayName = jsonArray.optJSONArray(i).getString(0);
                if (!displayName.equals(mGoogleSignInAccount.getDisplayName())) {
                    names.add(displayName);
                    UserLocation userLocation = locations.get(displayName);
                    if (userLocation == null) {
                        userLocation = new UserLocation(displayName);
                        locations.put(displayName, userLocation);
                    }
                    userLocation.setCoords(new LatLng(jsonArray.optJSONArray(i).getDouble(1), jsonArray.optJSONArray(i).getDouble(2)));
                    updatePositionOnMap(userLocation);
                }
            }

            Enumeration e = locations.keys();
            while (e.hasMoreElements()) {
                String displayName = (String) e.nextElement();
                if (!names.contains(displayName)) {
                    locations.get(displayName).getMarker().remove();
                    locations.remove(displayName);
                }
            }
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }
}
