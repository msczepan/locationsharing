package com.locationsharingapp.locationsharingapp;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by dstroynov on 16.12.15.
 */
public class PostCoordsTask extends AsyncTask<UserLocation, Integer, Boolean> {
    protected Boolean doInBackground(UserLocation... myLocation) {
        String dataUrl = "http://46.101.112.169/userlocations/" + URLEncoder.encode(myLocation[0].getDisplayName());
        String dataUrlParameters = "lat=" + myLocation[0].getCoords().latitude + "&lng=" + myLocation[0].getCoords().longitude;
        URL url;
        HttpURLConnection connection = null;

        try {
            url = new URL(dataUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(dataUrlParameters.getBytes().length));
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);


            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(dataUrlParameters);
            wr.flush();
            wr.close();

            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return true;
    }
}
