package com.locationsharingapp.locationsharingapp;

import org.json.JSONArray;

public interface OnGetCoordsTaskCompleted {
    void onGetCoordsTaskCompleted(JSONArray jsonArray);
}
