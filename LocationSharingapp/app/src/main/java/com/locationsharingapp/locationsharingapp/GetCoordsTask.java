package com.locationsharingapp.locationsharingapp;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;

/**
 * Created by dstroynov on 16.12.15.
 */
public class GetCoordsTask extends AsyncTask<UserLocations, Integer, JSONArray> {
    private OnGetCoordsTaskCompleted listener;

    public GetCoordsTask(OnGetCoordsTaskCompleted listener) {
        this.listener = listener;
    }

    protected JSONArray doInBackground(UserLocations... locations) {
        String dataUrl = "http://46.101.112.169/userlocations";
        URL url;
        HttpURLConnection connection = null;
        JSONArray jsonArray = null;
        try {
            url = new URL(dataUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setUseCaches(false);
            connection.setDoInput(true);

            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
            rd.close();
            jsonArray = new JSONArray(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return jsonArray;
    }

    protected void onPostExecute(JSONArray result) {
        if (result != null) {
            listener.onGetCoordsTaskCompleted(result);
        }
    }
}
